U ovom repozitorijumu se nalaze urađeni svi zadaci sa laboratorijskih vježbi iz
predmeta "Performanse računarskih sistema" na Elektrotehničkom fakultetu u
Banjaluci 2018. godine.

Svi projekti su rađeni u PyCharm razvojnom okruženju korištenjem programskog 
jezika Python.  GUI je razvijen u Qt aplikativnom frameworku korištenjem
QtDesigner razvojnog alata.

Za pokretanje aplikacija potrebni su:
 - python3 (preporučeno 3.6)
 - PyQt5 (preporučeno 5.10)
 - scipy (preporučeno 1.0.1)

Nadam se da je GUI razumljiv svima i da nije težak za upotrebu.

Poyy