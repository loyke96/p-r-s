import ChooseColumnRawDialog
from PyQt5 import QtWidgets

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = ChooseColumnRawDialog.ChooseColumnRawDialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
