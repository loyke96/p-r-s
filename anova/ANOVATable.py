import statistics
from math import sqrt

from PyQt5 import QtCore, QtWidgets
from scipy import stats


class ANOVATable(object):
    def setupUi(self, ANOVA: QtWidgets.QMainWindow, rows: int, columns: int) -> None:
        ANOVA.setObjectName("ANOVA")
        ANOVA.resize(800, 600)

        self.columns = columns
        self.rows = rows
        self.centralwidget = QtWidgets.QWidget(ANOVA)
        self.centralwidget.setObjectName("centralwidget")
        self.inputTable = QtWidgets.QTableWidget(self.centralwidget)
        self.inputTable.setGeometry(QtCore.QRect(20, 20, 550, 400))
        self.inputTable.setInputMethodHints(QtCore.Qt.ImhFormattedNumbersOnly)
        self.inputTable.setRowCount(rows)
        self.inputTable.setColumnCount(columns)
        self.inputTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.inputTable.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.inputTable.setObjectName("inputTable")
        self.buttonBox = QtWidgets.QDialogButtonBox(self.centralwidget)
        self.buttonBox.setGeometry(QtCore.QRect(20, 550, 550, 25))
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Reset)
        self.buttonBox.setObjectName("buttonBox")
        self.resultTable = QtWidgets.QTableWidget(self.centralwidget)
        self.resultTable.setGeometry(QtCore.QRect(20, 420, 550, 100))
        self.resultTable.setRowCount(2)
        self.resultTable.setColumnCount(columns)
        self.resultTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.resultTable.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.resultTable.setObjectName("resultTable")
        item = QtWidgets.QTableWidgetItem()
        self.resultTable.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.resultTable.setVerticalHeaderItem(1, item)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(585, 420, 200, 17))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.usvLabel = QtWidgets.QLabel(self.centralwidget)
        self.usvLabel.setGeometry(QtCore.QRect(585, 435, 200, 17))
        self.usvLabel.setText("")
        self.usvLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.usvLabel.setObjectName("usvLabel")
        self.sseLabel = QtWidgets.QLabel(self.centralwidget)
        self.sseLabel.setGeometry(QtCore.QRect(585, 40, 200, 17))
        self.sseLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.sseLabel.setObjectName("sseLabel")
        self.ssaLabel = QtWidgets.QLabel(self.centralwidget)
        self.ssaLabel.setGeometry(QtCore.QRect(585, 60, 200, 17))
        self.ssaLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.ssaLabel.setObjectName("ssaLabel")
        self.sstLabel = QtWidgets.QLabel(self.centralwidget)
        self.sstLabel.setGeometry(QtCore.QRect(585, 80, 200, 17))
        self.sstLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.sstLabel.setObjectName("sstLabel")
        self.sa2Label = QtWidgets.QLabel(self.centralwidget)
        self.sa2Label.setGeometry(QtCore.QRect(585, 150, 200, 17))
        self.sa2Label.setAlignment(QtCore.Qt.AlignCenter)
        self.sa2Label.setObjectName("sa2Label")
        self.se2Label = QtWidgets.QLabel(self.centralwidget)
        self.se2Label.setGeometry(QtCore.QRect(585, 170, 200, 17))
        self.se2Label.setAlignment(QtCore.Qt.AlignCenter)
        self.se2Label.setObjectName("se2Label")
        self.f1Label = QtWidgets.QLabel(self.centralwidget)
        self.f1Label.setGeometry(QtCore.QRect(585, 200, 200, 17))
        self.f1Label.setAlignment(QtCore.Qt.AlignCenter)
        self.f1Label.setObjectName("f1Label")
        self.f2Label = QtWidgets.QLabel(self.centralwidget)
        self.f2Label.setGeometry(QtCore.QRect(585, 220, 200, 17))
        self.f2Label.setAlignment(QtCore.Qt.AlignCenter)
        self.f2Label.setObjectName("f2Label")
        self.percentSpinBox = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.percentSpinBox.setGeometry(QtCore.QRect(654, 265, 62, 26))
        self.percentSpinBox.setMinimum(0.9)
        self.percentSpinBox.setMaximum(0.99)
        self.percentSpinBox.setSingleStep(0.01)
        self.percentSpinBox.setObjectName("percentSpinBox")
        self.ynLabel = QtWidgets.QLabel(self.centralwidget)
        self.ynLabel.setGeometry(QtCore.QRect(585, 242, 200, 17))
        self.ynLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.ynLabel.setObjectName("ynLabel")
        self.c1SpinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.c1SpinBox.setGeometry(QtCore.QRect(600, 265, 40, 26))
        self.c1SpinBox.setMinimum(1)
        self.c1SpinBox.setMaximum(columns)
        self.c1SpinBox.setObjectName("c1SpinBox")
        self.c2SpinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.c2SpinBox.setGeometry(QtCore.QRect(730, 265, 40, 26))
        self.c2SpinBox.setMinimum(1)
        self.c2SpinBox.setMaximum(columns)
        self.c2SpinBox.setObjectName("c2SpinBox")
        self.contrastLabel = QtWidgets.QLabel(self.centralwidget)
        self.contrastLabel.setGeometry(QtCore.QRect(590, 300, 190, 35))
        self.contrastLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.contrastLabel.setWordWrap(True)
        self.contrastLabel.setObjectName("f2Label")
        ANOVA.setCentralWidget(self.centralwidget)

        self.retranslateUi(ANOVA)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Reset).clicked.connect(self.resetAnova)
        self.buttonBox.rejected.connect(ANOVA.close)
        self.buttonBox.accepted.connect(self.calculateAnova)
        self.percentSpinBox.valueChanged.connect(self.tableFCalc)
        self.percentSpinBox.valueChanged.connect(self.contrastCalc)
        self.c1SpinBox.valueChanged.connect(self.contrastCalc)
        self.c2SpinBox.valueChanged.connect(self.contrastCalc)

        QtCore.QMetaObject.connectSlotsByName(ANOVA)

    def retranslateUi(self, ANOVA: QtWidgets.QMainWindow) -> None:
        _translate = QtCore.QCoreApplication.translate
        ANOVA.setWindowTitle(_translate("ANOVA", "ANOVA"))
        item = self.resultTable.verticalHeaderItem(0)
        item.setText(_translate("ANOVA", "Sredina"))
        item = self.resultTable.verticalHeaderItem(1)
        item.setText(_translate("ANOVA", "Efekat"))
        self.label.setText(_translate("ANOVA", "Ukupna srednja vrijednost:"))
        self.sseLabel.setText(_translate("ANOVA", "SSE:"))
        self.ssaLabel.setText(_translate("ANOVA", "SSA:"))
        self.sstLabel.setText(_translate("ANOVA", "SST:"))
        self.sa2Label.setText(_translate("ANOVA", "Sa^2:"))
        self.se2Label.setText(_translate("ANOVA", "Se^2:"))
        self.f1Label.setText(_translate("ANOVA", "Izračunato F:"))
        self.tableFCalc()

    def resetAnova(self) -> None:
        self.inputTable.clear()
        self.resultTable.clear()
        self.sstLabel.setText("SST:")
        self.sseLabel.setText("SSE:")
        self.ssaLabel.setText("SSA:")
        self.usvLabel.clear()
        self.f1Label.setText("Izračunato F:")
        self.ynLabel.clear()
        self.contrastLabel.clear()

    def tableFCalc(self) -> None:
        percent = self.percentSpinBox.value()
        dfn = self.columns - 1
        dfd = self.columns * (self.rows - 1)
        self.f_critical = stats.f._ppf(percent, dfn, dfd)
        self.f2Label.setText("Tabelarno F: " + "%.4f" % self.f_critical)
        self.tellYesNo()

    def calculateAnova(self) -> None:
        self.alternativeMeanCalc()
        totalMean = self.totalMeanCalc()
        self.effectCalc(totalMean)
        ssa = self.ssaCalc()
        sse = self.sseCalc()
        self.sstSet(ssa + sse)
        sa2 = self.sa2Calc(ssa)
        self.se2 = self.se2Calc(sse)
        self.fTest(sa2, self.se2)
        self.contrastCalc()

    def alternativeMeanCalc(self) -> None:
        for i in range(self.columns):
            values = []
            for j in range(self.rows):
                value = float(self.inputTable.item(j, i).text())
                values.append(value)
                columnMean = statistics.mean(values)
            cell = QtWidgets.QTableWidgetItem("%.4f" % columnMean)
            self.resultTable.setItem(0, i, cell)

    def totalMeanCalc(self) -> float:
        values = []
        for i in range(self.columns):
            value = float(self.resultTable.item(0, i).text())
            values.append(value)
        mean = statistics.mean(values)
        self.usvLabel.setText("%.4f" % mean)
        return mean

    def effectCalc(self, totalMean: float) -> None:
        for i in range(self.columns):
            mean = float(self.resultTable.item(0, i).text())
            cell = QtWidgets.QTableWidgetItem("%.4f" % (mean - totalMean))
            self.resultTable.setItem(1, i, cell)

    def ssaCalc(self) -> float:
        ssa = 0.0
        for i in range(self.columns):
            effect = float(self.resultTable.item(1, i).text())
            ssa += effect ** 2
        ssa *= self.rows
        self.ssaLabel.setText("SSA: " + "%.4f" % ssa)
        return ssa

    def sa2Calc(self, ssa: float) -> float:
        sa2 = ssa / (self.columns - 1)
        self.sa2Label.setText("Sa^2: " + "%.4f" % sa2)
        return sa2

    def sseCalc(self) -> float:
        sse = 0.0
        for i in range(self.columns):
            mean = float(self.resultTable.item(0, i).text())
            for j in range(self.rows):
                value = float(self.inputTable.item(j, i).text())
                sse += (value - mean) ** 2
        self.sseLabel.setText("SSE: " + "%.4f" % sse)
        return sse

    def se2Calc(self, sse: float) -> float:
        se2 = sse / (self.columns * (self.rows - 1))
        self.se2Label.setText("Se^2: " + "%.4f" % se2)
        return se2

    def sstSet(self, sst: float) -> None:
        self.sstLabel.setText("SST: " + "%.4f" % sst)

    def fTest(self, sa2: float, se2: float) -> None:
        self.f_tested = sa2 / se2;
        self.f1Label.setText("Izračunato F: " + "%.4f" % self.f_tested)
        self.tellYesNo()

    def tellYesNo(self) -> None:
        if self.f1Label.text() == "Izračunato F:":
            self.ynLabel.clear()
        elif self.f_tested < self.f_critical:
            self.ynLabel.setText("Razlike nisu značajne")
        else:
            self.ynLabel.setText("Razlike su značajne")

    def contrastCalc(self) -> None:
        if self.ynLabel.text():
            if self.c1SpinBox.value() != self.c2SpinBox.value():
                contrast = float(self.resultTable.item(1, self.c1SpinBox.value() - 1).text())
                contrast -= float(self.resultTable.item(1, self.c2SpinBox.value() - 1).text())
                cDeviation = sqrt(2 * self.se2 / (self.columns * self.rows))
                studentCritical = stats.t.ppf(self.percentSpinBox.value(), self.columns * (self.rows - 1))
                contrast1 = contrast - cDeviation * studentCritical
                contrast2 = contrast + cDeviation * studentCritical
                if contrast1 < 0 < contrast2:
                    self.contrastLabel.setText("Ne postoji razlika između odabranih alternativa")
                else:
                    self.contrastLabel.setText("Postoji razlika između odabranih alternativa")
            else:
                self.contrastLabel.clear()
