import ANOVATable
from PyQt5 import QtWidgets, QtCore


class ChooseColumnRawDialog(object):
    def setupUi(self, CCRDialog: QtWidgets.QDialog) -> None:
        CCRDialog.setObjectName("Dialog")
        CCRDialog.resize(400, 300)
        self.buttonBox = QtWidgets.QDialogButtonBox(CCRDialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Reset)
        self.buttonBox.setObjectName("buttonBox")
        self.widget = QtWidgets.QWidget(CCRDialog)
        self.widget.setGeometry(QtCore.QRect(100, 70, 191, 104))
        self.widget.setObjectName("widget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.brojAlternativaLine = QtWidgets.QLineEdit(self.widget)
        self.brojAlternativaLine.setObjectName("brojAlternativaLine")
        self.verticalLayout_2.addWidget(self.brojAlternativaLine)
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.brojMjerenjaLine = QtWidgets.QLineEdit(self.widget)
        self.brojMjerenjaLine.setObjectName("brojMjerenjaLine")
        self.verticalLayout_2.addWidget(self.brojMjerenjaLine)

        self.retranslateUi(CCRDialog)
        self.buttonBox.accepted.connect(self.drawTable)
        self.buttonBox.rejected.connect(CCRDialog.reject)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Reset).clicked.connect(self.resetValues)
        QtCore.QMetaObject.connectSlotsByName(CCRDialog)

    def retranslateUi(self, Dialog: QtWidgets.QMainWindow) -> None:
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "ANOVA"))
        self.label.setText(_translate("Dialog", "Broj Alternativa:"))
        self.label_2.setText(_translate("Dialog", "Broj Mjerenja:"))

    def resetValues(self) -> None:
        self.brojAlternativaLine.clear()
        self.brojMjerenjaLine.clear()

    def drawTable(self) -> None:
        alt = self.brojAlternativaLine.text()
        num = self.brojMjerenjaLine.text()
        self.anovaWindow = QtWidgets.QMainWindow()
        self.anovaUi = ANOVATable.ANOVATable()
        self.anovaUi.setupUi(self.anovaWindow, int(num), int(alt))
        self.anovaWindow.show()
