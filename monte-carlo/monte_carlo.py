import math
from os import linesep
from random import random

from PyQt5 import QtCore, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form: QtWidgets.QWidget) -> None:
        Form.setObjectName("Form")
        Form.resize(400, 300)
        Form.setStyleSheet("color: rgb(220, 20, 60);\n"
                           "background-color: rgb(255, 228, 196);")
        self.spinBox_decimale = QtWidgets.QSpinBox(Form)
        self.spinBox_decimale.setGeometry(QtCore.QRect(219, 40, 151, 26))
        self.spinBox_decimale.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.spinBox_decimale.setAlignment(QtCore.Qt.AlignCenter)
        self.spinBox_decimale.setMinimum(0)
        self.spinBox_decimale.setMaximum(10)
        self.spinBox_decimale.setProperty("value", 2)
        self.spinBox_decimale.setObjectName("spinBox_decimale")
        self.spinBox_iteracije = QtWidgets.QSpinBox(Form)
        self.spinBox_iteracije.setGeometry(QtCore.QRect(30, 40, 151, 26))
        self.spinBox_iteracije.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.spinBox_iteracije.setAlignment(QtCore.Qt.AlignCenter)
        self.spinBox_iteracije.setMinimum(10)
        self.spinBox_iteracije.setMaximum(2147483647)
        self.spinBox_iteracije.setProperty("value", 100000)
        self.spinBox_iteracije.setObjectName("spinBox_iteracije")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setEnabled(False)
        self.progressBar.setGeometry(QtCore.QRect(10, 270, 381, 23))
        self.progressBar.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                       "color: rgb(220, 20, 60);")
        self.progressBar.setObjectName("progressBar")
        self.pushButton_iteracije = QtWidgets.QPushButton(Form)
        self.pushButton_iteracije.setGeometry(QtCore.QRect(60, 70, 90, 25))
        self.pushButton_iteracije.setStyleSheet("background-color: rgb(255, 127, 80);")
        self.pushButton_iteracije.setObjectName("pushButton_iteracije")
        self.pushButton_decimale = QtWidgets.QPushButton(Form)
        self.pushButton_decimale.setGeometry(QtCore.QRect(250, 70, 90, 25))
        self.pushButton_decimale.setStyleSheet("background-color: rgb(255, 127, 80);")
        self.pushButton_decimale.setObjectName("pushButton_decimale")
        self.label_iteracije = QtWidgets.QLabel(Form)
        self.label_iteracije.setGeometry(QtCore.QRect(30, 130, 151, 81))
        self.label_iteracije.setText("")
        self.label_iteracije.setAlignment(QtCore.Qt.AlignCenter)
        self.label_iteracije.setWordWrap(True)
        self.label_iteracije.setObjectName("label_iteracije")
        self.label_decimale = QtWidgets.QLabel(Form)
        self.label_decimale.setGeometry(QtCore.QRect(220, 130, 151, 81))
        self.label_decimale.setText("")
        self.label_decimale.setAlignment(QtCore.Qt.AlignCenter)
        self.label_decimale.setWordWrap(True)
        self.label_decimale.setObjectName("label_decimale")

        self.retranslateUi(Form)

        self.pushButton_iteracije.clicked.connect(self.calculate_Pi_iteracije)
        self.pushButton_decimale.clicked.connect(self.calculate_Pi_decimale)
        self.spinBox_decimale.valueChanged.connect(self.decimale_suffix)
        self.spinBox_iteracije.valueChanged.connect(self.iteracije_suffix)

        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form: QtWidgets.QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Monte Carlo"))
        self.spinBox_decimale.setSuffix(_translate("Form", "  decimale"))
        self.spinBox_iteracije.setSuffix(_translate("Form", "  iteracija"))
        self.pushButton_iteracije.setText(_translate("Form", "Start"))
        self.pushButton_decimale.setText(_translate("Form", "Start"))

    def calculate_Pi_iteracije(self) -> None:
        broj_iteracija = self.spinBox_iteracije.value()
        brojac = 0
        for i in range(0, broj_iteracija):
            self.progressBar.setValue(i * 100 / broj_iteracija)
            x = random()
            y = random()
            if x * x + y * y <= 1:
                brojac += 4
        self.progressBar.setValue(100)
        self.label_iteracije.setText("Pi = " + str(brojac / broj_iteracija))

    def calculate_Pi_decimale(self) -> None:
        self.progressBar.setValue(0)
        epsilon = 0.1 ** (self.spinBox_decimale.value())
        nova_vrijednost = 2
        brojac = 0
        while nova_vrijednost < 2.5 or nova_vrijednost == 4:
            prethodna_vrijednost = -10
            while math.fabs(nova_vrijednost - prethodna_vrijednost) >= epsilon:
                prethodna_vrijednost = nova_vrijednost
                brojac += 1
                x = random()
                y = random()
                if x * x + y * y <= 1:
                    nova_vrijednost += (4 - nova_vrijednost) / brojac
                else:
                    nova_vrijednost -= nova_vrijednost / brojac
                razlika = math.fabs(nova_vrijednost - prethodna_vrijednost)
                if razlika != 0:
                    progres = epsilon * 100 / razlika
                    if progres > self.progressBar.value():
                        self.progressBar.setValue(epsilon * 100 / razlika)
        self.progressBar.setValue(100)
        self.label_decimale.setText("Pi = " + str(round(nova_vrijednost, self.spinBox_decimale.value())) + linesep +
                                    "Broj iteracija = " + str(brojac))

    def decimale_suffix(self) -> None:
        if 2 <= self.spinBox_decimale.value() <= 4:
            self.spinBox_decimale.setSuffix("  decimale")
        else:
            self.spinBox_decimale.setSuffix("  decimala")

    def iteracije_suffix(self) -> None:
        if self.spinBox_iteracije.value() % 100 not in range(10, 20) and \
                self.spinBox_iteracije.value() % 10 in range(2, 5):
            self.spinBox_iteracije.setSuffix("  iteracije")
        else:
            self.spinBox_iteracije.setSuffix("  iteracija")


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
