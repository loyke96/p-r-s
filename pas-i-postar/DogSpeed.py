from random import random


class DogSpeed(object):
    __slipProbability: float
    __speed: float
    __kind: str

    def __init__(self, kind: str) -> None:
        self.__kind = kind
        if kind == 'A':
            self.__speed = -0.01
        else:
            self.__speed = -0.015
        self.__slipProbability = 0

    def getCurrentSpeed(self) -> float:
        if self.__kind == 'A':
            return self.__kindOneSpeed()
        else:
            return self.__kindTwoSpeed()

    def __slip(self) -> bool:
        self.__slipProbability += 0.00001
        if random() < self.__slipProbability:
            self.__slipProbability = 0
            return True
        return False

    def __kindOneSpeed(self) -> float:
        self.__speed += 0.02
        return self.__speed if self.__speed < 10 else 10

    def __kindTwoSpeed(self) -> float:
        if self.__slip():
            self.__speed = 0.015
            return self.__speed
        self.__speed += 0.03
        return self.__speed if self.__speed < 12 else 12
