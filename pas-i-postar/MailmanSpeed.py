class MailmanSpeed(object):
    __time: float
    __kind: str

    def __init__(self, kind: str) -> None:
        self.__kind = kind
        self.__time = 0

    def getMailmanSpeed(self) -> float:
        if self.__kind == 'A':
            return self.__kindOneSpeed()
        else:
            return self.__kindTwoSpeed()

    def __kindOneSpeed(self) -> float:
        if self.__time < 3:
            self.__time += 0.01
            return 0
        else:
            return 5

    def __kindTwoSpeed(self) -> float:
        return 2.5
