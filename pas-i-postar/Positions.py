from math import fabs, sqrt
from random import random
from typing import Tuple

from DogSpeed import DogSpeed
from MailmanSpeed import MailmanSpeed


class Positions(object):
    __mailmanSpeed: MailmanSpeed
    __dogSpeed: DogSpeed
    __time: float
    __mailmanPosition: Tuple[float, float]
    __dogPosition: Tuple[float, float]

    def __init__(self, dogSpeed: DogSpeed, mailmanSpeed: MailmanSpeed) -> None:
        self.__dogSpeed = dogSpeed
        self.__mailmanSpeed = mailmanSpeed
        self.__dogPosition = (0, 20)
        x_pos = random() * 50
        print('X = ' + str(x_pos))
        self.__mailmanPosition = (x_pos, 0)
        self.__time = 0

    def getTime(self) -> float:
        return self.__time

    def getDogPosition(self) -> Tuple[float, float]:
        return self.__dogPosition

    def setDogPosition(self, dogPosition: Tuple[float, float]) -> None:
        self.__dogPosition = dogPosition

    def newDogPosition(self) -> Tuple[float, float]:
        self.__time += 0.01
        x_distance = self.__mailmanPosition[0] - self.__dogPosition[0]
        y_distance = self.__mailmanPosition[1] - self.__dogPosition[1]
        distance = sqrt(x_distance ** 2 + y_distance ** 2)
        dogSpeed = self.__dogSpeed.getCurrentSpeed()
        x_distance *= dogSpeed / distance * 0.01
        y_distance *= dogSpeed / distance * 0.01
        self.__dogPosition = (self.__dogPosition[0] + x_distance, self.__dogPosition[1] + y_distance)
        return self.__dogPosition

    def getMailmanPosition(self) -> Tuple[float, float]:
        return self.__mailmanPosition

    def setMailmanPosition(self, mailmanPosition: Tuple[float, float]) -> None:
        self.__mailmanPosition = mailmanPosition

    def newMailmanPosition(self) -> Tuple[float, float]:
        self.__mailmanPosition = (self.__mailmanPosition[0] + self.__mailmanSpeed.getMailmanSpeed() / 100, 0)
        return self.__mailmanPosition

    def checkCollision(self) -> bool:
        x_distance = fabs(self.__mailmanPosition[0] - self.__dogPosition[0])
        y_distance = fabs(self.__mailmanPosition[1] - self.__dogPosition[1])
        return x_distance ** 2 + y_distance ** 2 < 0.005
