from PyQt5 import QtCore, QtWidgets
import Simulation_MainWindow


class Choose_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setStyleSheet("color: rgb(46, 139, 87);\n"
                             "background-color: rgb(152, 251, 152);")
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setStyleSheet("background-color: rgb(46, 139, 87);\n"
                                     "color: rgb(152, 251, 152);")
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(90, 90, 211, 81))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.comboBoxMailman = QtWidgets.QComboBox(self.widget)
        self.comboBoxMailman.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.comboBoxMailman.setObjectName("comboBoxMailman")
        self.comboBoxMailman.addItem("")
        self.comboBoxMailman.addItem("")
        self.verticalLayout.addWidget(self.comboBoxMailman)
        self.comboBoxDog = QtWidgets.QComboBox(self.widget)
        self.comboBoxDog.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.comboBoxDog.setObjectName("comboBoxDog")
        self.comboBoxDog.addItem("")
        self.comboBoxDog.addItem("")
        self.verticalLayout.addWidget(self.comboBoxDog)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(self.openSimulationWindow)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "PAS I POŠTAR"))
        self.comboBoxMailman.setItemText(0, _translate("Dialog", "Veži pertle"))
        self.comboBoxMailman.setItemText(1, _translate("Dialog", "Ostavi pertle"))
        self.comboBoxDog.setCurrentText(_translate("Dialog", "Čivava"))
        self.comboBoxDog.setItemText(0, _translate("Dialog", "Čivava"))
        self.comboBoxDog.setItemText(1, _translate("Dialog", "Staford"))

    def openSimulationWindow(self):
        dogKind = 'A'
        mailmanKind = 'A'
        if self.comboBoxDog.currentIndex() == 1:
            dogKind = 'B'
        if self.comboBoxMailman.currentIndex() == 1:
            mailmanKind = 'B'
        self.simulationWindow = QtWidgets.QMainWindow()
        self.swUi = Simulation_MainWindow.Simulation_MainWindow()
        self.swUi.setupUi(self.simulationWindow, dogKind, mailmanKind)
        self.simulationWindow.show()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Choose_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
