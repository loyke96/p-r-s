from time import sleep

from DogSpeed import DogSpeed
from MailmanSpeed import MailmanSpeed
from Positions import Positions
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import qApp, QGraphicsScene


class Simulation_MainWindow(object):
    def setupUi(self, MainWindow, dogKind, mailmanKind):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("background-color: rgb(152, 251, 152);\n"
                                         "color: rgb(0, 255, 127);")
        self.centralwidget.setObjectName("centralwidget")
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(0, 0, 801, 601))
        self.graphicsView.setObjectName("graphicsView")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 0, 801, 601))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("mailman1.jpg"))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(340, 520, 88, 34))
        self.pushButton.setStyleSheet("color: rgb(152, 251, 152);\n"
                                      "background-color: rgb(46, 139, 87);")
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.dogKind = dogKind
        self.mailmanKind = mailmanKind

        self.retranslateUi(MainWindow)
        self.pushButton.clicked.connect(self.startSimulation)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PAS I POŠTAR"))
        self.pushButton.setText(_translate("MainWindow", "START"))

    def startSimulation(self):
        for i in range(3, 0, -1):
            self.pushButton.setText(str(i))
            qApp.processEvents()
            sleep(1)
        self.pushButton.hide()
        self.label.hide()
        qApp.processEvents()
        positions = Positions(DogSpeed(self.dogKind), MailmanSpeed(self.mailmanKind))
        scene = QGraphicsScene()
        self.graphicsView.setScene(scene)
        while True:
            (x, y) = positions.getDogPosition()
            scene.addEllipse(x * 10, y * 10, 4, 4, Qt.red)
            (x, y) = positions.getMailmanPosition()
            scene.addEllipse(x * 10, y * 10, 4, 4, Qt.blue)
            qApp.processEvents()
            positions.newMailmanPosition()
            positions.newDogPosition()
            if positions.checkCollision():
                break
            sleep(0.01)
        self.pushButton.setText("START")
        self.label.setPixmap(QtGui.QPixmap("mailman2.jpg"))
        self.label.show()
        self.pushButton.show()
        print('Proteklo vrijeme: ' + str(positions.getTime()))
